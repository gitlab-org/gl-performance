# GitLab SiteSpeed Exporter

This is a SiteSpeed plugin which exports specific results into a JSON file that can be parsed by GitLab's [Browser Performance Testing](https://docs.gitlab.com/ee/ci/testing/browser_performance_testing.html) feature.

This plugin outputs a JSON in the following format. These are all the supported metrics as of now:

```json
[  
  {
    "subject": "/",
    "metrics": [
      {
        "name": "Total Score",
        "value": 72,
        "desiredSize": "larger"
      },
      {
        "name": "Requests",
        "value": 16,
        "desiredSize": "smaller"
      },
      {
        "name": "Speed Index",
        "value": 1879,
        "desiredSize": "smaller"
      },
      {
        "name": "First Contentful Paint",
        "value": 998,
        "desiredSize": "smaller"
      },
      {
        "name": "Largest Contentful Paint",
        "value": 2434,
        "desiredSize": "smaller"
      },
      {
        "name": "Total Blocking Time",
        "value": 1000,
        "desiredSize": "smaller"
      },
      {
        "name": "Transfer Size (KB)",
        "value": "779.9",
        "desiredSize": "smaller"
      }
    ]
  }
]
```

## Contributing to the GitLab SiteSpeed Exporter

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.
